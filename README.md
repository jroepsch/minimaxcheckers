# MinimaxCheckers

Checkers against the computer using minimax algorithm.  This was initially created in the Fall 2018 HackISU challenge over 8 working hours, not including sleep/etc.

## Running
```python2.7 logic/Game.py```

Alternatively, you can just run it in an IDE that is able to run Python.  I've been using pydev for Eclipse, and this allows you to use ctrl+c to copy your next move.

## Gameplay
You are the black side, represented by b or B.  Your options for the next move are shown below the board, and can be copied and pasted to be sent as your move. Standard checkers rules apply, such as forcing you to make a jump if possible.

## Future Plans
* Clean up code/comments
* Create new functions to reduce repeating code/complexity
