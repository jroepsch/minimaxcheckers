from Board import *
from Piece import *
from Moves import *

'''
Created on Oct 20, 2018

@author: Justin Roepsch
'''
def alphabeta(board, depth, maxDepth, alpha, beta, mySide, curSide, prevMoves):
    if(depth==maxDepth or isLeaf(board, curSide)):
        #board.printBoard()
        score = [evaluateBoard(board, mySide),prevMoves]
        #print(score)
        return score
    if(curSide==mySide):
        maxVal=[-1000,""]
        for move in AllMovesPossible(board, curSide):
            newMoves=copy.deepcopy(prevMoves)
            newMoves.append(move)
            val=alphabeta(genBoardWholeTurn(board, move),depth+1,maxDepth,alpha,beta, mySide, (curSide+1)%2, newMoves)
            if(val[0]>maxVal[0]):
                maxVal=copy.deepcopy(val)
            #maxVal=max(maxVal, val[0])
            alpha=max(alpha,maxVal[0])
            if(beta<=alpha):
                break
        return maxVal
    else:
        minVal=[1000,""]
        for move in AllMovesPossible(board, curSide):
            newMoves=copy.deepcopy(prevMoves)
            newMoves.append(move)
            #newMoves=prevMoves.append(move)
            val=alphabeta(genBoardWholeTurn(board, move),depth+1,maxDepth,alpha,beta, mySide, (curSide+1)%2, newMoves)
            if(val[0]<minVal[0]):
                minVal=copy.deepcopy(val)
            #minVal=min(minVal, val)
            beta=min(beta,minVal[0])
            if(beta<=alpha):
                break
        return minVal
        
        