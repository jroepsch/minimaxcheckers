from Board import *
from Moves import *
from minimax import *
'''
Created on Oct 19, 2018

@author: Justin Roepsch
'''

if __name__ == '__main__':
    b = Board()
    b.printBoard()
    newBoard=copy.deepcopy(b)
    while(True):
        yourMoves=AllMovesPossible(newBoard, 1)
        if(yourMoves==[] or didSideLose(newBoard, 1)):
            print("You lost!")
            break
        text = raw_input("Your options are: %s\n"%(AllMovesPossible(newBoard, 1)))
        arr=text.split("->")
        move=Move([])
        for pos in arr:
            move.extend(int(pos[1]), int(pos[4]))
        print(move)
        newBoard=copy.deepcopy(newBoard)
        newBoard=genBoardWholeTurn(newBoard, move)
        
        newBoard.printBoard()
        if(didSideLose(newBoard, 0)):
            print("You won!")
            break
        val1=alphabeta(newBoard, 0, 5, -1000, 1000, 0, 0, [])
        val2 = val1[1][0]
        print("CPU uses %s"%(val2))

        newBoard=genBoardWholeTurn(newBoard, val2)
        newBoard.printBoard()
        
    
    pass