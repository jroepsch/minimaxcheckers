import copy
from Board import *

'''
Created on Oct 19, 2018

@author: Justin Roepsch
'''
class Move(object):
    #startx=0
    #starty=0
    #endx=0
    #endy=0
    allMoves=[]
    def __init__(self, allMoves):
        #self.startx=startx
        #self.starty=starty
        #self.endx=endx
        #self.endy=endy
        self.allMoves=allMoves

    def __repr__(self):
        str="(%d, %d)"%(self.startx(), self.starty())
        for iter in range(len(self.allMoves)):
            if(iter!=0):
                str+="->(%d, %d)"%(self.allMoves[iter][0], self.allMoves[iter][1])
        return str
        #return ("(%d, %d)->(%d, %d)"%(self.startx(), self.starty(), self.endx(), self.endy()))
    def __str__(self):
        return ("(%d, %d)->(%d, %d)"%(self.startx(), self.starty(), self.endx(), self.endy()))
    
    def extend(self, anotherx, anothery):
        self.allMoves.append([anotherx, anothery])
        #self.endx=anotherx
        #self.endy=anothery
    def endx(self):
        length=len(self.allMoves)
        return self.allMoves[length-1][0]
    def endy(self):
        length=len(self.allMoves)
        return self.allMoves[length-1][1]
    def startx(self):
        return self.allMoves[0][0]
    def starty(self):
        return self.allMoves[0][1]
    def prevx(self):
        length=len(self.allMoves)
        return self.allMoves[length-2][0]
    def prevy(self):
        length=len(self.allMoves)
        return self.allMoves[length-2][1]

def AllMovesPossible(board,side):
    finalPossibleJumps=[]
    finalJumps=False
    finalPossible=[]
    for y in range(8):
        for x in range(8):
            if(board.playableSpot(x,y) and board.b[x][y] != 0 and board.b[x][y].Owner==side):
                PossibleMoves=ValidMoveDirections(board, x, y)
                PossibleJumps=ValidJumpDirections(board, x, y)
                #print("moves for %d %d"%(x,y))
                #print(PossibleMoves)
                #print(PossibleJumps)
                ForcedMoves=[]
                if(PossibleJumps!=[]):
                    finalJumps=True
                    for jump in PossibleJumps:
                        formattedJump=Move(0)
                        formattedJump.allMoves=[[x,y],[jump[0],jump[1]]]
                        newJumps=continuedJumps(genBoard(board, formattedJump),formattedJump,jump[0],jump[1])
                        if(not isinstance(newJumps, Move)):
                            newJumps=newJumps[0]
                        if(isinstance(newJumps, Move)):
                           ForcedMoves.append(newJumps)
                        else:
                            ForcedMoves.extend(newJumps)
                    finalPossible.extend(ForcedMoves)
                    #if(not isinstance(ForcedMoves[0], Move)):
                        #ForcedMoves=ForcedMoves[0]
                    
                else:
                    for move in PossibleMoves:
                       formattedMove=Move(0)
                       formattedMove.allMoves=[[x,y],[move[0],move[1]]]
                       ForcedMoves.append(formattedMove)
                    finalPossibleJumps.extend(ForcedMoves)
    if(not finalJumps):
        finalPossible.extend(finalPossibleJumps)
                    #ForcedMoves=PossibleMoves
                
                #print(ForcedMoves)    
                #finalPossible.extend(ForcedMoves)
    
    #print finalPossible
    return finalPossible

#Needs to be able to recursively go into itself.  Given a list of possible jumps.  Looks at each, sees if there are jumps after each that can be made.  Returns an array of the new forced jumps
def continuedJumps(board, movesSoFar, x, y):
    PossibleJumps=ValidJumpDirections(board, movesSoFar.endx(), movesSoFar.endy())
    MoreMoves=[]
    
    for move in PossibleJumps:
        copyMove=Move(0)
        copyMove.allMoves=copy.deepcopy(movesSoFar.allMoves)
        copyMove.extend(move[0], move[1])
        #appendedMoves=[]
        #appendedMoves.append(movesSoFar.extend(move))
        #appendedMoves.append(move)
        if((move[1]==0 or move[1]==7) and not board.b[x][y].isKing):# dont continue recursively
            MoreMoves.append(copyMove)
        else:
            thisx=copyMove.endx()
            MoreMoves.append(continuedJumps(genBoard(board, copyMove),copyMove,copyMove.endx(),copyMove.endy()))
        #print("something")
        
        
        '''partialMoves=ValidJumpDirections(board, move.newx, move.newy)
        if(partialMoves!=[]):
            for partial in partialMoves:
                
            MoreMoves.append(continuedJumps(generatedBoard(board), movesSoFar, x, y))
          '''  
        #if(move.endy==7 or move.endy==0):
    if(MoreMoves==[]):
        MoreMoves.append(movesSoFar)        
    return MoreMoves
        
        
    print("no")


#def generateBoardFromJumps(board, jumps):
def genBoardWholeTurn(board, listOfMoves):
    tempBoard=copy.deepcopy(board)
    if(isinstance(listOfMoves, Move)):
        iter=0
        for move in listOfMoves.allMoves:
            if(iter!=0):
                tempBoard.b[listOfMoves.allMoves[iter][0]][listOfMoves.allMoves[iter][1]]=Piece(tempBoard.b[listOfMoves.allMoves[iter-1][0]][listOfMoves.allMoves[iter-1][1]].Owner,tempBoard.b[listOfMoves.allMoves[iter-1][0]][listOfMoves.allMoves[iter-1][1]].isKing)
                if(abs(listOfMoves.allMoves[iter-1][0]-listOfMoves.allMoves[iter][0])==2):
                    tempBoard.b[(listOfMoves.allMoves[iter-1][0]+listOfMoves.allMoves[iter][0])/2][(listOfMoves.allMoves[iter-1][1]+listOfMoves.allMoves[iter][1])/2]=0
                if(listOfMoves.allMoves[iter][1]==0 or listOfMoves.allMoves[iter][1]==7):
                    tempBoard.b[listOfMoves.allMoves[iter][0]][listOfMoves.allMoves[iter][1]].isKing=True
                tempBoard.b[listOfMoves.allMoves[iter-1][0]][listOfMoves.allMoves[iter-1][1]]=0
            iter+=1
    return tempBoard
        


def genBoard(board, listOfMoves):
    tempBoard=copy.deepcopy(board)
    '''if(isinstance(listOfMoves, Move)):
        iter=0
        for move in listOfMoves.allMoves:
            if(iter!=0):
                tempBoard.b[listOfMoves.allMoves[iter][0]][listOfMoves.allMoves[iter][1]]=Piece(tempBoard.b[listOfMoves.allMoves[iter-1][0]][listOfMoves.allMoves[iter-1][1]].Owner,tempBoard.b[listOfMoves.allMoves[iter-1][0]][listOfMoves.allMoves[iter-1][1]].isKing)
                if(abs(listOfMoves.allMoves[iter-1][0]-listOfMoves.allMoves[iter][0])==2):
                    tempBoard.b[(listOfMoves.allMoves[iter-1][0]+listOfMoves.allMoves[iter][0])/2][(listOfMoves.allMoves[iter-1][1]+listOfMoves.allMoves[iter][1])/2]=0
                if(listOfMoves.allMoves[iter][1]==0 or listOfMoves.allMoves[iter][1]==7):
                    tempBoard.b[listOfMoves.allMoves[iter][0]][listOfMoves.allMoves[iter][1]].isKing=True
                tempBoard.b[listOfMoves.allMoves[iter-1][0]][listOfMoves.allMoves[iter-1][1]]=0
            iter+=1
    return tempBoard'''
    #for y in range(8):
    #    for x in range(8):
    #       tempBoard.b[x][y]=0
    #       if(board.b[x][y]!=0):
    #           tempBoard.b[x][y]=Piece(board.b[x][y].Owner, board.b[x][y].isKing)
    #I now have a brand new copy of the board
    if(isinstance(listOfMoves, Move)):
        #print(listOfMoves.endx)
        #print(listOfMoves.endy)
        tempBoard.b[listOfMoves.endx()][listOfMoves.endy()]=Piece(tempBoard.b[listOfMoves.prevx()][listOfMoves.prevy()].Owner,tempBoard.b[listOfMoves.prevx()][listOfMoves.prevy()].isKing)
        if(abs(listOfMoves.prevx()-listOfMoves.endx())==2):
            tempBoard.b[(listOfMoves.prevx()+listOfMoves.endx())/2][(listOfMoves.prevy()+listOfMoves.endy())/2]=0
        if(listOfMoves.endy()==0 or listOfMoves.endy()==7):
            tempBoard.b[listOfMoves.endx()][listOfMoves.endy()].isKing=True
        tempBoard.b[listOfMoves.prevx()][listOfMoves.prevy()]=0
        
        
        '''iter=0
        for move in listOfMoves.allMoves:
            if(iter!=0):
                tempBoard.b[listOfMoves.endx()][listOfMoves.endy()]=Piece(tempBoard.b[listOfMoves.prevx()][listOfMoves.prevy()].Owner,tempBoard.b[listOfMoves.prevx()][listOfMoves.prevy()].isKing)
                if(abs(listOfMoves.prevx()-listOfMoves.endx())==2):
                    tempBoard.b[(listOfMoves.prevx()+listOfMoves.endx())/2][(listOfMoves.prevy()+listOfMoves.endy())/2]=0
                if(listOfMoves.endy()==0 or listOfMoves.endy()==7):
                    tempBoard.b[listOfMoves.endx()][listOfMoves.endy()].isKing=True
                tempBoard.b[listOfMoves.startx()][listOfMoves.starty()]=0
            iter+=1'''
    else:
        iter=0
        for move in listOfMoves:
            if(iter!=0):
                tempBoard.b[move[0]][move[1]]=Piece(tempBoard.b[listOfMoves[iter-1][0]][listOfMoves[iter-1][1]].Owner,tempBoard.b[listOfMoves[iter-1][0]][listOfMoves[iter-1][1]].isKing)
                if(abs(move.prevx()-move.endx())==2):
                    tempBoard.b[(move.prevx()+move.endx())/2][(move.prevy()+move.endy())/2]=0
                if(move[1]==0 or move[1]==7):
                    tempBoard.b[move[0]][move[1]].isKing=True
            tempBoard.b[move.prevx()][move.prevy()]=0

                
    return tempBoard
        
        
def genBoardTurns(board, listOfTurns):
    tempBoard=board
    for turn in listOfTurns:
        finalBoard=genBoard(tempBoard, turn)
    return tempBoard


def ValidMoveDirections(board, x, y):
    returner=[]
    if(x>0):
        if(y>0):
            if(board.b[x-1][y-1] ==0 and(board.b[x][y].Owner==1 or board.b[x][y].isKing)):
                returner.append([x-1,y-1])
        if(y<7):
            if(board.b[x-1][y+1] ==0 and(board.b[x][y].Owner==0 or board.b[x][y].isKing)):
                returner.append([x-1,y+1])
    if(x<7):
        if(y>0):
            if(board.b[x+1][y-1] ==0 and(board.b[x][y].Owner==1 or board.b[x][y].isKing)):
                returner.append([x+1,y-1])
        if(y<7):
            if(board.b[x+1][y+1] ==0 and(board.b[x][y].Owner==0 or board.b[x][y].isKing)):
                returner.append([x+1,y+1])
    return returner

def ValidJumpDirections(board, x, y):
    returner=[]
    if(x>1):
        if(y>1):
            if(board.b[x-1][y-1] !=0 and board.b[x-2][y-2] ==0):
                if(board.b[x-1][y-1].Owner!=board.b[x][y].Owner and(board.b[x][y].Owner==1 or board.b[x][y].isKing)):
                    returner.append([x-2,y-2])
        if(y<6):
            if(isinstance(board.b[x-1][y+1], Piece)and not isinstance(board.b[x-2][y+2], Piece)):
                if(board.b[x-1][y+1].Owner!=board.b[x][y].Owner and(board.b[x][y].Owner==0 or board.b[x][y].isKing)):
                    returner.append([x-2,y+2])
    if(x<6):
        if(y>1):
            if(board.b[x+1][y-1] !=0 and board.b[x+2][y-2] ==0):
                if(board.b[x+1][y-1].Owner!=board.b[x][y].Owner and(board.b[x][y].Owner==1 or board.b[x][y].isKing)):
                    returner.append([x+2,y-2])
        if(y<6):
            #board.printBoard()
            if(isinstance(board.b[x+1][y+1], Piece) and not isinstance(board.b[x+2][y+2], Piece)):
                if(board.b[x+1][y+1].Owner!=board.b[x][y].Owner and(board.b[x][y].Owner==0 or board.b[x][y].isKing)):
                    returner.append([x+2,y+2])
    return returner

def isLeaf(board, curSide):
    if(AllMovesPossible(board, curSide)==[]):
        return True
    return False
    
    #if(abs(movesSoFar.prevx()-movesSoFar.endx())==1):
    #    return True
    
    
    
    
    
    
    
