from Piece import *
import sys

'''
Created on Oct 19, 2018

@author: Justin Roepsch
'''
from array import array

class Board(object):
    '''
    classdocs
    '''
    b=[]

    def __init__(self):
        '''
        Constructor
        '''
        self.b=[[0 for x in range(8)] for y in range(8)]
        for y in range(8):
            for x in range(8):
                if(self.playableSpot(x, y)):
                    if(y<3):
                        self.b[x][y]=Piece(0,False)
                        #print(x, y, '0')
                    if(y>4):
                        self.b[x][y]=Piece(1,False)
                        #print(x, y, '1')
                    
                    
                
            
            
            
            
    def __repr__(self):
        str ="\n"
        str+=("|   | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |\n")
        for y in range(8):
            str+=("| %s |" %(y))
            for x in range(8):
                
                if(self.playableSpot(x, y) and self.b[x][y]!=0):
                    if(self.b[x][y].Owner==0 and self.b[x][y].isKing):
                        str+=(" B |")
                    if(self.b[x][y].Owner==1 and self.b[x][y].isKing):
                        str+=(" W |")
                    if(self.b[x][y].Owner==0 and not self.b[x][y].isKing):
                        str+=(" b |")
                    if(self.b[x][y].Owner==1 and not self.b[x][y].isKing):
                        str+=(" w |")
                else:
                    str+=("   |")
            str+=("\n_____________________________________\n")       
        return str
            
            
            
    def playableSpot(self, x, y): 
        if((x+y)%2==1):
            return True
        return False       
            
    def printBoard(self):
        print("|   | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |")
        for y in range(8):
            sys.stdout.write("| %s |" %(y))
            for x in range(8):
                
                if(self.playableSpot(x, y) and self.b[x][y]!=0):
                    if(self.b[x][y].Owner==0 and self.b[x][y].isKing):
                        sys.stdout.write(" B |")
                    if(self.b[x][y].Owner==1 and self.b[x][y].isKing):
                        sys.stdout.write(" W |")
                    if(self.b[x][y].Owner==0 and not self.b[x][y].isKing):
                        sys.stdout.write(" b |")
                    if(self.b[x][y].Owner==1 and not self.b[x][y].isKing):
                        sys.stdout.write(" w |")
                else:
                    sys.stdout.write("   |")
            print("\n_____________________________________")
        

def evaluateBoard(board, side):
    score = 0
    noEnemy=True
    for y in range(8):
            for x in range(8):
                if(isinstance(board.b[x][y],Piece)):
                    if(board.b[x][y].Owner==side and board.b[x][y].isKing):
                        score+=2
                    if(board.b[x][y].Owner==side and not board.b[x][y].isKing):
                        score+=1
                    if(board.b[x][y].Owner!=side and board.b[x][y].isKing):
                        score-=2  
                        noEnemy=False 
                    if(board.b[x][y].Owner!=side and not board.b[x][y].isKing):
                        score-=1
                        noEnemy=False
    if(noEnemy):
        return 500
    return score
    #print("The score is %d"%(score))
    #return score

def didSideLose(board, side):
    
    for y in range(8):
        for x in range(8):
            if(isinstance(board.b[x][y],Piece)):
                if(board.b[x][y].Owner==side):
                    return False
    return True








